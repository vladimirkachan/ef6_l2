﻿using System;
using System.Data.Entity;
using System.Linq;

namespace L2_4
{
    public class OneToMany : DbContext
    {
        public OneToMany()
            : base("name=OneToMany")
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Order> Orders { get; set; }

    }
}