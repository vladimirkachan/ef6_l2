﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<OneToMany>());
            using OneToMany db = new();
            var products = new List<Product>
            {
                new() {Name = "Nokia Lumia 930", Price = 9000}, new() {Name = "Nokia Lumia 830", Price = 6000},
                new() {Name = "Samsung Galaxy S5", Price = 10000}, new() {Name = "Samsung Galaxy S4", Price = 6000},
                new() {Name = "LG 5", Price = 8000}
            };
            db.Products.AddRange(products);
            db.SaveChanges();
            foreach (var p in db.Products.ToList()) Console.WriteLine($"{p.Id}. {p.Name} ${p.Price} Customer: {p.Order?.Customer}");
            Console.WriteLine("--------------------------");
            Order order1 = new Order { Customer = "Nazar", Quantity = 2, Product = new List<Product> { products[0], products[2]} };
            Order order2 = new () { Customer = "Alex", Quantity = 1, Product = new List<Product> { products[1], products[3] } };
            db.Orders.AddRange(new List<Order> { order1, order2 });
            db.SaveChanges();
            foreach (var item in db.Products.ToList()) Console.WriteLine("{0}.{1} - {2} ({3})", item.Id, item.Name, item.Price, item.Order != null ? item.Order.Customer : "Not Customer");
            Console.WriteLine("-----------------------------------------");
            foreach (var order in db.Orders.ToList())
            {
                Console.WriteLine("{0}.{1}", order.Id, order.Customer);
                if (order.Product == null) continue;
                foreach (var product in order.Product) 
                    Console.WriteLine("{0} - {1} * {2} = {3}", product.Name, product.Price, order.Quantity, product.Price * order.Quantity);
                Console.WriteLine("-----------------------------------------");
            }
            Console.ReadKey();
        }
    }
}
