﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace L2_2
{
    public partial class Form1 : Form
    {
        ProductDb db = new();
        public Form1()
        {
            InitializeComponent();
        }
        protected override void OnLoad(EventArgs e)
        {
            db.Products.Load();
            bindingSource1.DataSource = db.Products.Local.ToBindingList();
        }
        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            try
            {
                db.SaveChanges();
            }
            finally
            {
                db.Dispose();
            }
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string error = "", s = e.FormattedValue.ToString();
            switch (e.ColumnIndex)
            {
                case 1:
                    if (s == "") error = @"'Name' must not be empty!";
                    break;
                case 2:
                    if (!decimal.TryParse(s, out var money) || money <= 0)
                        error = @"'Price' can only be a positive number";
                    break;
            }
            e.Cancel = error != "";
            dataGridView1.Rows[e.RowIndex].ErrorText = error;
        }

        private void dataGridView1_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            db.SaveChanges();
            dataGridView1.Invalidate();
        }
    }
}
