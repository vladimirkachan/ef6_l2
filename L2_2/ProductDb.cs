﻿using System;
using System.Data.Entity;
using System.Linq;

namespace L2_2
{
    public class ProductDb : DbContext
    {
        public ProductDb() : base("name=ProductDb") {}

        public virtual DbSet<Product> Products { get; set; }
    }
}