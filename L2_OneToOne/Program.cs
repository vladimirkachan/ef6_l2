﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_OneToOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<OneToOneEntity>());
            using OneToOneEntity db = new();
            //Generate(db);
            //Create(db, "lena_17", "abc", "Elena", 20);
            foreach(var u in db.Users.ToList())
                Console.WriteLine($"login: {u.Login}, password: {u.Password}, {u.Profile.Name} {u.Profile.Age} years old");
            Console.ReadKey();
        }
        static void Generate(OneToOneEntity db)
        {
            User vova = new() {Login = "vova", Password = "111", Profile = new UserProfile {Name = "Vladimir", Age = 33}},
                 natasha = new() {Login = "natali", Password = "222", Profile = new UserProfile {Name = "Natalia", Age = 22}};
            db.Users.AddRange(new[] {vova, natasha});
            db.SaveChanges();
        }
        static void Create(OneToOneEntity db, string login, string password, string name, int age)
        {
            db.Users.Add(new User
            {
                Login = login, Password = password, Profile = new UserProfile {Name = name, Age = age}
            });
            db.SaveChanges();
        }
    }
}
