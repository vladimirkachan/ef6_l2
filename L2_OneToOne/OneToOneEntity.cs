﻿using System;
using System.Data.Entity;
using System.Linq;

namespace L2_OneToOne
{
    public class OneToOneEntity : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }

        public OneToOneEntity()
            : base("name=OneToOneEntity")
        {
        }

    }

}