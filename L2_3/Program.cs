﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<OneToOne>());
            using OneToOne db = new();
            Generate(db);
            foreach(var o in db.Orders.ToList())
                Console.WriteLine($"{o.Id}. {o.Customer} → {o.Product?.Name}: {o.Quantity} pcs at ${o.Product?.Price}");
            Console.ReadKey();
        }
        static void Generate(OneToOne db)
        {
            Order a = new() {Customer = "China", Quantity = 20, Product = new Product{ Name = "MIG-35", Price = 45 } }, 
                  b = new() {Customer = "Great Rus", Quantity = 33, Product = new Product{ Name = "TU-160", Price = 220 } };
            db.Orders.AddRange(new[] {a, b});
            db.SaveChanges();
        }
    }
}
