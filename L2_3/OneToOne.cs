﻿using System;
using System.Data.Entity;
using System.Linq;

namespace L2_3
{
    public class OneToOne : DbContext
    {
        public DbSet<Order> Orders {get; set;}
        public DbSet<Product> Products {get; set;}

        public OneToOne() : base("name=OneToOne") {}

    }
}