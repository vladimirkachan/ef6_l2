﻿using System;
using System.Data.Entity;
using System.Linq;

namespace L2_5
{
    public class ManyToMany : DbContext
    {
        public ManyToMany()
            : base("name=ManyToMany")
        {
        }

        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
    }
}