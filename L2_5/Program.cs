﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_5
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<ManyToMany>());
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ManyToManyDbEntity>());

            using ManyToMany db = new();
            Product p1 = new () { Name = "Nokia Lumia 930", Price = 9000 };
            Product p2 = new () { Name = "Nokia Lumia 830", Price = 6000 };
            Product p3 = new () { Name = "Samsung Galaxy S5", Price = 10000 };
            Product p4 = new () { Name = "Samsung Galaxy S4", Price = 6000 };
            Product p5 = new() {Name = "LG Nexus 5", Price = 3000};

            db.Products.AddRange(new List<Product> { p1, p2, p3, p4 });
            db.SaveChanges();

            Order order1 = new () { Customer = "Nazar", Quantity = 1 };
            order1.Product.Add(p1);
            order1.Product.Add(p2);
            order1.Product.Add(p4);

            Order order2 = new () { Customer = "Alex", Quantity = 2 };
            order2.Product.Add(p1);
            order2.Product.Add(p3);
            order2.Product.Add(p4);
            order2.Product.Add(p5);

            db.Orders.AddRange(new [] { order1, order2 });
            db.SaveChanges();

            foreach (var product in db.Products.Include(p => p.Order))
            {
                Console.WriteLine("\t{0}.{1}", product.Id, product.Name);

                if (product.Order == null) continue;
                foreach (var order in product.Order) Console.WriteLine("{0}.{1}", order.Id, order.Customer);
                Console.WriteLine("-----------------------------------------");
            }

            Console.WriteLine("\t* * * * *");

            foreach (var order in db.Orders.Include(p => p.Product))
            {
                Console.WriteLine("\t{0}.{1}", order.Id, order.Customer);

                if (order.Product == null) continue;

                foreach (var product in order.Product) 
                    Console.WriteLine("{0}.{1} - {2} | Sum: {3} x {2} = {4}", product.Id, product.Name, 
                                      product.Price, order.Quantity, product.Price * order.Quantity);
                Console.WriteLine("-----------------------------------------");
            }
            Console.ReadKey();
        }
    }
}
