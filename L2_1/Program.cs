﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace L2_1
{
    class Program
    {
        static void Main(string[] args)
        {

            const string providerName = "System.Data.SqlClient";
            const string serverName = @".";
            string databasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
            @"\EF6_L2_1.mdf";

            // Initialize the connection string builder for the
            // underlying provider.
            var sqlBuilder = new SqlConnectionStringBuilder
            {
                DataSource = serverName,
                AttachDBFilename = databasePath,
                IntegratedSecurity = true
            };

            // Build the SqlConnection connection string.
            string providerString = sqlBuilder.ToString();

            // Initialize the EntityConnectionStringBuilder.
            var entityBuilder = new EntityConnectionStringBuilder
            {
                Provider = providerName,
                ProviderConnectionString = providerString,
                Metadata = @"res://*/Model1.csdl|res://*/Model1.ssdl|res://*/Model1.msl"
            };

            Console.WriteLine(entityBuilder.ToString());

            using (var conn = new EntityConnection(entityBuilder.ToString()))
            {
                conn.Open();
                Console.WriteLine("Just testing the connection.");
                conn.Close();
            }

            Console.ReadKey();
        }
    }
}
